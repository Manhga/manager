<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class QTLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = 'Admin')
    {
        if (Auth::guard($guard)->check()) {
            return redirect('Admin/home');
        }
        else{
            return redirect(url('Admin/login'));
        }

    }
}
