<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use App\Models\Users;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function login(){
        return view('login');
    }

    // public function postLogin(Request $req)
    // {
    //     $validate = \Validator::make($req->all(),[
    //         'email'=>'required',
    //         'password'=>'required|min:8|max:42'
            
    //     ],
    //     [
    //         'email.required'=>'Bạn chưa nhập email',
    //         'password.required'=>'Bạn chưa nhập password',
    //         'password.min'=>'password phải chứa ít nhất 8 ký tự',
    //         'password.max'=>'password chỉ được từ 8~42 ký tự'
    //     ]);
    //     if($validate->fails())
    //     {
    //         return response()->json([
    //             'error'=>true,
    //             'message'=>$validate->errors()
    //         ],200);
    //     }
    //     else{
    //         $email = $req->emial;
    //         $password = $req->password;
    //         if(Auth::attempt(['email'=>$email, 'password'=>$password],true))
    //         {
    //             $model['arr'] = Users::where('id',Auth::id());
    //             foreach($model['arr'] as $key)
    //             {
    //                 if($key->model_id == 1)
    //                 {
    //                     if($key->status == 0)
    //                     {
    //                         return view('Nhanvien/blades/home');
    //                     }
    //                     else{
    //                         return view('auth/password/reset');
    //                     }
    //                 }
    //             }
               
            
    //         }
    //         else
    //         {
    //             $errors = new MessageBag(['errorLogin'=>'Email hoặc mật khẩu không đúng']);
    //             return response()->json([
    //                 'error'=>true,
    //                 'mesage'=>$errors
    //             ],200);
    //         }
    //     }
    // }


    public function creatAcc()
    {
        return view('creatAcc');
    }
    // public function postCreatAcc(Request $req)
    // {
    //     $validate = \validator::make([
    //         'name'=>'required',
    //         'email'=>'required|unique:users,email',
    //         'model'=>'required'
    //     ],
    //     [
    //         'name.required'=>'Tên nhân viên hoặc quản lý chưa được nhập',
    //         'email.required'=>'Bạn chưa nhập email',
    //         'email.uniqued'=>'Email đã tồn tại trong hệ thống',
    //         'model.required'=>'Bạn chưa chọn loại quyền',
    //     ]);
    //     if($validate->fails())
    //     {
    //         return response()->json([
    //             'error'=>true,
    //             'mesage'=>$validate->errors()
    //         ]);
    //     }
    //     else {
    //         $user = new Users;
    //         $user->name = $req->name;
    //         $user->email = $req->email;
    //         $user->password = bcrypt(123456789);
    //         $user->status = 1;
    //         $user->save();
    //         return response()->json(['success'=>'Thành công']);
    //     }
    // }
}
