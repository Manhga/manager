<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use App\Models\Users;
class PageController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    
    public function login(){
        return view('login');
    }

    public function postLogin(Request $req)
    {
        $validate = \Validator::make($req->all(),[
            'email'=>'required',
            'password'=>'required|min:8|max:42'
            
        ],
        [
            'email.required'=>'Bạn chưa nhập email',
            'password.required'=>'Bạn chưa nhập password',
            'password.min'=>'password phải chứa ít nhất 8 ký tự',
            'password.max'=>'password chỉ được từ 8~42 ký tự'
        ]);
        if($validate->fails())
        {
            return response()->json([
                'error'=>true,
                'message'=>$validate->errors()
            ],200);
        }
        else{
            // $password = $req->password;
            // $data = Users::all()->get();
            // foreach ($data as $key) {
            // 	if($data->email == $req->email)
            // 	{
            // 		$model_id = $data->model_id;
            // 	}
            // }
            if(Auth::attempt(['email'=>$req->email, 'password'=>$req->password],true))
            {
                $abc = Users::where('id',Auth::id())->get();
                foreach($abc as $key)
                {
                    if($key->model_id == 1)
                    {
                        if($key->status == 0)
                        {
                        	$success = new MessageBag(['success1'=>$key->status]);
                            return response()->json([
                            	'error'=>false,
                            	'message'=>$success,
                            ],200);
                        }
                        else{
                        	$success = new MessageBag(['success1'=>$key->status]);
                            return response()->json([
                            	'error'=>false,
                            	'message'=>$success,
                            ],200);
                        }
                    }
                }
            }
            else
            {
                $errors = new MessageBag(['errorlogin'=>'Email hoặc mật khẩu không đúng']);
                return response()->json([
                    'error'=>true,
                    'message'=>$errors,
                ],200);
            }
        }
    }

    public function creatAcc()
    {
        return view('creatAcc');
    }
    public function postCreatAcc(Request $req)
    {
        $validate = \validator::make([
            'name'=>'required',
            'email'=>'required|unique:users,email',
            'model'=>'required'
        ],
        [
            'name.required'=>'Tên nhân viên hoặc quản lý chưa được nhập',
            'email.required'=>'Bạn chưa nhập email',
            'email.uniqued'=>'Email đã tồn tại trong hệ thống',
            'model.required'=>'Bạn chưa chọn loại quyền',
        ]);
        if($validate->fails())
        {
            return response()->json([
                'error'=>true,
                'mesage'=>$validate->errors()
            ]);
        }
        else {
            $user = new Users;
            $user->name = $req->name;
            $user->email = $req->email;
            $user->password = bcrypt(123456789);
            $user->status = 1;
            $user->save();
            return response()->json(['success'=>'Thành công']);
        }
    }
    public function Home(){
    	return view('QuanTri/home');
    }
    public function getNhanVien()
    {
    	$nhanvien = Users::where('model_id',0)->get();
    	// dd($nhanvien);
    	return view('QuanTri/blades/listnhanvien',['nhanvien'=>$nhanvien]);
    }
}
