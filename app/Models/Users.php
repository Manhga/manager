<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';
    public function has_users()
    {
    	return $this->belongsTo('App\Models\Users','user_id','id');
    }
}
