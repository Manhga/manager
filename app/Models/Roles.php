<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';
   	public function hasRoles()
   	{
   		return $this->belongsTo('App\Models\hasRoles','role_id','id');
   	}
}
