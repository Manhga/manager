<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hasRoles extends Model
{
    protected $table = 'model_has_roles';
    public function roles()
    {
    	return $this->hasMany('App\Models\Roles','role_id','id');
    }
}
