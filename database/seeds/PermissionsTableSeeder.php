<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
        	'name' => 'Write',
        	'guard_name'=> 'Write',
        ]);
        DB::table('permissions')->insert([
            'name' =>'Read',
            'guard_name' => 'Read',
        ]);
        DB::table('permissions')->insert([
            'name' => 'CreateUser',
            'guard_name' => 'CreateUser',
        ]);
        DB::table('permissions')->insert([
        	'name' => 'editUser',
            'guard_name' => 'editUser',
        ]);
        DB::table('permissions')->insert([
        	'name' => 'editPassUser',
            'guard_name' => 'editPassUser',
        ]);
    }
}
