<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
        	'name' => 'Author',
        	'guard_name'=> 'Author',
        ]);
        DB::table('roles')->insert([
            'name' =>'Admin',
            'guard_name' => 'Admin',
        ]);
        DB::table('roles')->insert([
            'name' => 'Leader',
            'guard_name' => 'Leader'
        ]);
    }
}
