@extends('QuanTri.layouts.index')
@section('content')
<div id="page-wrapper">
   <div class="row">
      <div class="col-lg-12">
         <h1 class="page-header">Tables</h1>
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-default">
            <div class="panel-heading">

               Danh sách nhân viên công ty
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
               <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                  <thead>
                     <tr>
                        <th>STT</th>
                        <th width="30%">Họ tên </th>
                        <th>Chức vụ</th>
                        <th width="20%">Thời gian làm việc</th>
                        <th width="20%">Team</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php $stt = 0; ?>
                     @foreach($nhanvien as $key)
                     <tr class="odd gradeX">
                        <td>{{$stt+1}}</td>
                        <td>{{$key->name}}</td>
                        <td class="center">{{$key->chucvu_id}}</td>
                        <td class="center"></td>
                        <td class="center"></td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
               <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
   <div class="well">
      <h2>Nhân viên theo các team</h2>
   </div>
   <div class="row">
      <div class="col-lg-6">
         <div class="panel panel-default">
            <div class="panel-heading">
               Kitchen Sink
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
               <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>First Name</th>
                           <th>Last Name</th>
                           <th>Username</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
      </div>
      <!-- /.col-lg-6 -->
      <div class="col-lg-6">
         <div class="panel panel-default">
            <div class="panel-heading">
               Basic Table
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
               <div class="table-responsive">
                  <table class="table">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>First Name</th>
                           <th>Last Name</th>
                           <th>Username</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>1</td>
                           <td>Mark</td>
                           <td>Otto</td>
                           <td>@mdo</td>
                        </tr>
                        <tr>
                           <td>2</td>
                           <td>Jacob</td>
                           <td>Thornton</td>
                           <td>@fat</td>
                        </tr>
                        <tr>
                           <td>3</td>
                           <td>Larry</td>
                           <td>the Bird</td>
                           <td>@twitter</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
      </div>
      <!-- /.col-lg-6 -->
   </div>
   <!-- /.row -->
   <div class="row">
      <div class="col-lg-6">
         <div class="panel panel-default">
            <div class="panel-heading">
               Striped Rows
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
               <div class="table-responsive">
                  <table class="table table-striped">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>First Name</th>
                           <th>Last Name</th>
                           <th>Username</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>1</td>
                           <td>Mark</td>
                           <td>Otto</td>
                           <td>@mdo</td>
                        </tr>
                        <tr>
                           <td>2</td>
                           <td>Jacob</td>
                           <td>Thornton</td>
                           <td>@fat</td>
                        </tr>
                        <tr>
                           <td>3</td>
                           <td>Larry</td>
                           <td>the Bird</td>
                           <td>@twitter</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
      </div>
      <!-- /.col-lg-6 -->
      <div class="col-lg-6">
         <div class="panel panel-default">
            <div class="panel-heading">
               Bordered Table
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
               <div class="table-responsive table-bordered">
                  <table class="table">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>First Name</th>
                           <th>Last Name</th>
                           <th>Username</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>1</td>
                           <td>Mark</td>
                           <td>Otto</td>
                           <td>@mdo</td>
                        </tr>
                        <tr>
                           <td>2</td>
                           <td>Jacob</td>
                           <td>Thornton</td>
                           <td>@fat</td>
                        </tr>
                        <tr>
                           <td>3</td>
                           <td>Larry</td>
                           <td>the Bird</td>
                           <td>@twitter</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
      </div>
      <!-- /.col-lg-6 -->
   </div>
   <!-- /.row -->
   <!-- /.row -->
</div>
@endsection