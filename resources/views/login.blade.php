<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta  name="_token" content="{{csrf_token()}}"/>

    <title>Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{asset('vendor/metisMenu/metisMenu.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{asset('dist/css/sb-admin-2.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Đăng nhập</h3>
                    </div>
                    
                    <div class="panel-body">
                        <form role="form">
                            <input type="hidden" name="_token" class="token" value="{{csrf_token()}}">
                            <div class="alert alert-danger error errorLogin" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p style="color:red; display:none;" class="error errorLogin"></p>
                        </div>
                            <fieldset>
                                
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" id="email" type="email" autofocus>
                                    <p style="color:red; display: none" class="error errorEmail"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" id="password" name="password" type="password" value="">
                                    <p style="color:red; display: none" class="error errorPassword"></p>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button class="btn btn-lg btn-success btn-block" id="login">Đăng nhập</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#login').click(function(e){
                e.preventDefault();
                $.ajaxSetup({
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{url('Admin/login')}}",
                    method: 'post',
                    data:{
                        email: $('#email').val(),
                        password:  $('#password').val(),
                    },
                    success:function(data) {
                        console.log(data);
                        if(data.error == true)
                        {
                            $('.error').hide();
                            if(typeof data.message.email !== 'undefined')
                            {
                                $('.errorEmail').show().text(data.message.email[0]);
                            }
                            else
                            if(typeof data.message.password !== 'undefined')
                            {
                                $('.errorPassword').show().text(data.message.password[0]);
                            }
                            else if(typeof data.message.errorlogin !== 'undefined')
                            {
                                $('.errorLogin').show().text(data.message.errorlogin[0]);
                            }
                        }
                        else
                        {
                            console.log(data);
                            if(typeof data.message.success1 !== 'undefined')
                            {

                                if(data.message.success1[0] === 1)
                                {
                                   window.location.href= "{{url('QuanTri/Home')}}"; 
                                }
                                 else
                                {
                                    window.location.href= "{{url('Nhanvien/Home')}}";
                                }
                            }
                           
                        }
                    }
                })
            });
        });
    </script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{asset('vendor/metisMenu/metisMenu.min.js')}}"></script>

    <!-- Custom Theme JavaScript -->
    <!-- <script src="{{asset('dist/js/sb-admin-2.js')}}"></script> -->

</body>

</html>
